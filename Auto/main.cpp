#include <iostream>
using namespace std;

class Auto{
    float m_km;
    float m_cenaDen;
    float m_zarobok;
    float m_povodneKm;
public:
    Auto(float km, float cenaDen, float Zarobok){
        m_povodneKm= km;
        m_cenaDen= cenaDen;
        m_zarobok= Zarobok;;
        m_km=0.0;
    }

    float getPozicovne(float pocetDni){
        return pocetDni*m_cenaDen;
    }

    void evidujPozicku(float preslokm, float pocetDni){
        m_km += preslokm;
        m_zarobok+= getPozicovne(pocetDni);

    }

    void zmenaCeny(){
        if ((m_km - m_povodneKm) > 10000) {
            m_cenaDen -= m_cenaDen * 0.10;
            m_povodneKm += m_km;
            cout<< "KM: "<< m_povodneKm<< endl;
            cout<< "Nova cena: "<< m_cenaDen<< endl;
            cout<< "Novy zarobok: "<< m_zarobok<< endl;
        }
        else printInf();
    }
    void printInf(){
            cout<< "KM: "<<m_povodneKm << endl;
            cout<< "cena za den: "<< m_cenaDen<< endl;
            cout<< "Zarobok auta: "<< m_zarobok<< endl;
        }

};


int main() {
    Auto*Ford= new Auto(5000,800,0);
    Ford ->printInf() ;
    Ford ->evidujPozicku(20500,3);
    Ford ->zmenaCeny();
    delete Ford;
    return 0;
}
