//
// Created by Grend on 12. 12. 2020.
//

#ifndef PROJEKT_HRAC_H
#define PROJEKT_HRAC_H

#include <iostream>
#include <vector>
#include "Mapa.h"
#include "Interakcia.h"
#include "Inventar.h"
#include "Miesnost.h"
class Interakcia;

class Hrac {
    std::vector<Interakcia*> m_interakcia;
    std::vector<Inventar*> m_inventar;
    std::string m_meno;
    int m_vek;
    std::string m_pohlavie;
public:

    void privitanie();
    void printInfo();







};


#endif //PROJEKT_HRAC_H
