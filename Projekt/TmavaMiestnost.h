//
// Created by Grend on 16. 12. 2020.
//

#ifndef PROJEKT_TMAVAMIESTNOST_H
#define PROJEKT_TMAVAMIESTNOST_H
#include "Svetelnost.h"

class TmavaMiestnost: public Svetelnost {
    public:
        void setMeno();
        void setPovecNabytku();
        int getcheckS();
};


#endif //PROJEKT_TMAVAMIESTNOST_H
