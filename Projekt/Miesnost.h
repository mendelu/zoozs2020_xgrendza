//
// Created by Grend on 16. 12. 2020.
//

#ifndef PROJEKT_MIESNOST_H
#define PROJEKT_MIESNOST_H
#include <iostream>
#include "Inventar.h"
#include <algorithm>

class Miesnost {
protected:
    std::string m_meno;
    int m_pocetNabytku;
public:
    Miesnost(std::string meno, int pocetNabytku);
    void printInfo();


};


#endif //PROJEKT_MIESNOST_H
