//
// Created by Grend on 12. 12. 2020.
//

#include "Mapa.h"
void Mapa::printMap() {
    using namespace std;
    cout<<"      ___________________________________"<<endl;
    cout<<"      |       |   |~~~~~~~~~|   :*      |"<<endl;
    cout<<"      |   K  *:   |~~~~~~~~~|   |  T2   |"<<endl;
    cout<<"      |_______|   |~~~~~~~~~|   |_______|"<<endl;
    cout<<"      |      *:   |_________|   :*      |"<<endl;
    cout<<"      |   T1  |                 |  S1   |"<<endl;
    cout<<"      |_______|   ______________|_______|"<<endl;
    cout<<"      |       |   |~~~~~~~~~~~~~~~~~~~~~|"<<endl;
    cout<<"Vychod:       :   |~~~~~~~~~~~~~~~~~~~~~|"<<endl;
    cout<<"      |_______|___|_____________________|"<<endl;
}