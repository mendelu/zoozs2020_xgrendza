//
// Created by Grend on 16. 12. 2020.
//

#ifndef PROJEKT_INTERAKCIA_H
#define PROJEKT_INTERAKCIA_H

#include <iostream>

class Interakcia {
     std::string m_popis;
public:
    Interakcia (std::string popis);
    std::string getPopis();
};



#endif //PROJEKT_INTERAKCIA_H
