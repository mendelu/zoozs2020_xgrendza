//
// Created by Grend on 16. 12. 2020.
//

#ifndef PROJEKT_SVETLAMIESTNOST_H
#define PROJEKT_SVETLAMIESTNOST_H
#include "Svetelnost.h"

class SvetlaMiestnost: public Svetelnost {
public:
    void setMeno();
    void setPovecNabytku();
    int getcheckS();

};


#endif //PROJEKT_SVETLAMIESTNOST_H
