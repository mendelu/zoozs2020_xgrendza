#include <iostream>
using namespace std;

class E_car{
    float m_maxAh;
    float m_availableAh;
    float m_maxCurrent;
public:
    E_car(float maxAh, float availableAh, float maxCurrent){
        m_maxAh= maxAh;
        m_availableAh= availableAh;
        m_maxCurrent= maxCurrent;
    }
    float getMaxCurrent(){
        return m_maxCurrent;
    }

    void charge (float maxAh){
        if((m_availableAh+maxAh)<m_maxAh){
            m_availableAh+= maxAh;
        }
        else {
            m_availableAh= m_maxAh;
        }
    }
    void printInfo(){
        cout<< "Max: "<<m_maxAh<<endl;
        cout<< "Current: "<<m_availableAh<<endl;
    }

};
class PowerStation{
    float m_maxCurrent;
    float m_hourChargeAh;
public:
    PowerStation(float maxcurrent, float hourChargeAh){
        m_maxCurrent= maxcurrent;
        m_hourChargeAh= hourChargeAh;
    }

    void chargeForHour(E_car*car){
        float charge= (car->getMaxCurrent()/m_maxCurrent)* m_hourChargeAh;
        car->charge(charge);
    }
};

int main() {
    PowerStation* ps1= new PowerStation(4,4);
    E_car* skoda= new E_car(20,10,2);
    ps1-> chargeForHour(skoda);
    skoda-> printInfo();




    delete ps1;
    delete skoda;
}
